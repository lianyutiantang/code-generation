package cn.lianyutiantang.base;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.converts.MySqlTypeConvert;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.DbColumnType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
/**
 * @author zhengjl
 * @ClassName MpGenerator
 * @Description 代码生成器演示
 * @Date 2018/9/1616:51
 * @Version 1.0
 **/
public class MpGenerator {
    /**
     * <p>
     * MySQL 生成演示
     * </p>
     */
    public static void main(String[] args) {
        AutoGenerator mpg = new AutoGenerator();
        // 选择 freemarker 引擎，默认 Veloctiy
        // mpg.setTemplateEngine(new FreemarkerTemplateEngine());

        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        gc.setOutputDir("D://generatorSourceCode");
        gc.setFileOverride(true);
        gc.setActiveRecord(true);// 不需要ActiveRecord特性的请改为false
        gc.setEnableCache(false);// XML 二级缓存
        gc.setBaseResultMap(true);// XML ResultMap
        gc.setBaseColumnList(false);// XML columList
        //gc.setKotlin(true);// 是否生成 kotlin 代码
        gc.setAuthor("zhengjl");
//        gc.setEntityName("%sEntity");
        // 自定义文件命名，注意 %s 会自动填充表实体属性！
        gc.setMapperName("%sDao");
        // gc.setXmlName("%sDao");
        // gc.setServiceName("MP%sService");
        // gc.setServiceImplName("%sServiceDiy");
        // gc.setControllerName("%sAction");
        mpg.setGlobalConfig(gc);

        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setDbType(DbType.MYSQL);
        dsc.setTypeConvert(new MySqlTypeConvert(){
            // 自定义数据库表字段类型转换【可选】
            @Override
            public DbColumnType processTypeConvert(GlobalConfig globalConfig, String fieldType) {
                System.out.println("转换类型：" + fieldType);
                return super.processTypeConvert(globalConfig, fieldType);
            }
        });
        dsc.setDriverName("com.mysql.jdbc.Driver");
        dsc.setUsername("root");
        dsc.setPassword("zhengJL&wczz@748");
        dsc.setUrl("jdbc:mysql://193.112.69.216:3306/trade-user?characterEncoding=utf8");
//        dsc.setUrl("jdbc:mysql://127.0.0.1:3306/trade?characterEncoding=utf8");
        mpg.setDataSource(dsc);

        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        // strategy.setCapitalMode(true);// 全局大写命名 ORACLE 注意
       // strategy.setTablePrefix(new String[] { "tlog_", "tsys_" });// 此处可以修改为您的表前缀
        strategy.setNaming(NamingStrategy.underline_to_camel);// 表名生成策略
        /**
         * 获取需要生成代码的表
         SELECT
         GROUP_CONCAT(CONCAT('"', t.TABLE_NAME, '"'))
         FROM information_schema.TABLES t
         WHERE t.TABLE_SCHEMA = 'trade-user'
         AND t.TABLE_NAME LIKE 'ut_%'
         */
        //strategy.setInclude(new String[] { "ut_charge_details","ut_customer_info","ut_follow_up_record","ut_linkman","ut_open_ticket","ut_store_processing_info","ut_supplier_info","ut_transportation_info"}); // 需要生成的表
//        strategy.setInclude(new String[] {"sm_materials_stock_my","sm_materials_stock_sup","sm_product_stock_my","sm_product_stock_sup","sm_stock_adjustment_cpk"}); // 需要生成的表
//        strategy.setInclude(new String[] {"fm_bank_account_info","fm_capital_flow_info","fm_income_invoice_detail","fm_income_invoice_info","fm_ot_payable_detail","fm_ot_payable_info","fm_ot_receivable_detail","fm_ot_receivable_info","fm_outcome_invoice_detail","fm_outcome_invoice_info","fm_payable_detail","fm_payable_info","fm_receivable_detail","fm_receivable_info"}); // 需要生成的表
        strategy.setInclude(new String[] {"sys_resource_tab"}); // 需要生成的表
        // strategy.setExclude(new String[]{"test"}); // 排除生成的表
        // 自定义实体父类
        // strategy.setSuperEntityClass("com.baomidou.demo.TestEntity");
        // 自定义实体，公共字段
        // strategy.setSuperEntityColumns(new String[] { "test_id", "age" });
        // 自定义 mapper 父类
        // strategy.setSuperMapperClass("com.baomidou.demo.TestMapper");
        // 自定义 service 父类
        // strategy.setSuperServiceClass("com.baomidou.demo.TestService");
        // 自定义 service 实现类父类
        // strategy.setSuperServiceImplClass("com.baomidou.demo.TestServiceImpl");
        // 自定义 controller 父类
        // strategy.setSuperControllerClass("com.baomidou.demo.TestController");
        // 【实体】是否生成字段常量（默认 false）
        // public static final String ID = "test_id";
        // strategy.setEntityColumnConstant(true);
        // 【实体】是否为构建者模型（默认 false）
        // public User setName(String name) {this.name = name; return this;}
        // strategy.setEntityBuilderModel(true);
        mpg.setStrategy(strategy);

        // 包配置
        PackageConfig pc = new PackageConfig();
        pc.setParent("cn.lianyutiantang");//设置代码java下面的目录
//        pc.setModuleName("");//设置模块名
//        pc.setParent("cn.lianyutiantang");//设置代码java下面的目录
//        pc.setModuleName("tradeapifinancial");//财务模块api
//        pc.setModuleName("tradeimplfinancial");//财务模块impl
//        pc.setModuleName("tradeapifileserver");//设置模块名
//        pc.setModuleName("tradeimplfileserver");//设置模块名
//        pc.setModuleName("tradeimplunit");//设置模块名
//        pc.setModuleName("tradeapisysmgt");//设置模块名
//        pc.setModuleName("tradeapisysmgt");//设置模块名
        pc.setModuleName("tradeimplsysmgt");//设置模块名
//        pc.setModuleName("tradeimplstock");//设置模块名
        mpg.setPackageInfo(pc);

        // 注入自定义配置，可以在 VM 中使用 cfg.abc 【可无】
    /*    InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("abc", this.getConfig().getGlobalConfig().getAuthor() + "-mp");
                this.setMap(map);
            }
        };*/

        // 自定义 xxList.jsp 生成
     /*   List<FileOutConfig> focList = new ArrayList<FileOutConfig>();
        focList.add(new FileOutConfig("/template/list.jsp.vm") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输入文件名称
                return "D://generatorSourceCode//template//my_" + tableInfo.getEntityName() + ".jsp";
            }
        });
        cfg.setFileOutConfigList(focList);
        mpg.setCfg(cfg);*/

        // 调整 xml 生成目录演示
      /*  focList.add(new FileOutConfig("/templates/mapper.xml.vm") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                return "/develop/code/xml/" + tableInfo.getEntityName() + ".xml";
            }
        });
        cfg.setFileOutConfigList(focList);
        mpg.setCfg(cfg);*/

        // 关闭默认 xml 生成，调整生成 至 根目录
       /* TemplateConfig tc = new TemplateConfig();
        tc.setXml(null);
        mpg.setTemplate(tc);*/

        // 自定义模板配置，可以 copy 源码 mybatis-plus/src/main/resources/templates 下面内容修改，
        // 放置自己项目的 src/main/resources/templates 目录下, 默认名称一下可以不配置，也可以自定义模板名称
         TemplateConfig tc = new TemplateConfig();
        // tc.setController("...");
        // tc.setEntity("...");
        // tc.setMapper("...");
        // tc.setXml("...");
        // tc.setService("...");
        // tc.setServiceImpl("...");
        // 如上任何一个模块如果设置 空 OR Null 将不生成该模块。
         mpg.setTemplate(tc);

        // 执行生成
        mpg.execute();

        // 打印注入设置【可无】
       // System.err.println(mpg.getCfg().getMap().get("abc"));
    }
}
