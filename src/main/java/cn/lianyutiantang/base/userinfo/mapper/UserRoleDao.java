package cn.lianyutiantang.base.userinfo.mapper;

import cn.lianyutiantang.base.userinfo.entity.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhengjl
 * @since 2018-09-16
 */
public interface UserRoleDao extends BaseMapper<UserRole> {

}
