package cn.lianyutiantang.base.userinfo.mapper;

import cn.lianyutiantang.base.userinfo.entity.Deparment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhengjl
 * @since 2018-09-16
 */
public interface DeparmentDao extends BaseMapper<Deparment> {


    List listPage(Page paramPage, Map map);



}
