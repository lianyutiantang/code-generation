package cn.lianyutiantang.base.userinfo.controller;


import cn.lianyutiantang.base.userinfo.service.IDeparmentService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zhengjl
 * @since 2018-09-16
 */
@Controller
@RequestMapping("/deparment")
public class DeparmentController {
    @Autowired
    IDeparmentService iDeparmentService;



    @ResponseBody
    @GetMapping("/testTransactional")
    public Object testTransactional(){
        iDeparmentService.testTransactional();
        return null;
    }
    @ResponseBody
    @GetMapping("/testTransactionalLcn")
    public Object testTransactionalLcn(){
        iDeparmentService.testTransactionalLcn();
        return null;
    }

    @ResponseBody
    @GetMapping("/listPage")
    public Object listPage(String pageNo,String pageSize){
        Page paramPage = new Page<>(Long.parseLong(pageNo),Long.parseLong(pageSize));
        Map map = new HashMap();
        paramPage =iDeparmentService.listPage(paramPage,map);
        return paramPage;
    }




}

