package cn.lianyutiantang.base.userinfo.service;

import cn.lianyutiantang.base.userinfo.entity.Deparment;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhengjl
 * @since 2018-09-16
 */
public interface IDeparmentService extends IService<Deparment> {

    void testTransactional();


    Page listPage(Page paramPage, Map map);

    void testTransactionalLcn();


}
