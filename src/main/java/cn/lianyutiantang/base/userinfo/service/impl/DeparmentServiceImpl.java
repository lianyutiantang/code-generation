package cn.lianyutiantang.base.userinfo.service.impl;

import cn.lianyutiantang.base.commons.CloudClientUtil;
import cn.lianyutiantang.base.tuser.model.UserDomain;
import cn.lianyutiantang.base.tuser.service.UserService;
import cn.lianyutiantang.base.userinfo.entity.Deparment;
import cn.lianyutiantang.base.userinfo.entity.UserRole;
import cn.lianyutiantang.base.userinfo.mapper.DeparmentDao;
import cn.lianyutiantang.base.userinfo.service.IDeparmentService;
import cn.lianyutiantang.base.userinfo.service.IUserRoleService;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
//import com.codingapi.tx.annotation.TxTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhengjl
 * @since 2018-09-16
 */
@RestController
public class DeparmentServiceImpl extends ServiceImpl<DeparmentDao, Deparment> implements IDeparmentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeparmentServiceImpl.class);
    @Autowired
    IUserRoleService iUserRoleService;
    @Autowired
    UserService userService;

    @Override
    @Transactional
    public void testTransactional() {
        Deparment deparment1=new Deparment();
        deparment1.setName("部门1");
        this.save(deparment1);
        UserRole userRole1=new UserRole();
        userRole1.setRolesId(1l);
        userRole1.setUserId(2l);
        userRole1.setName("用户角色name1");
        userRole1.setText("用户角色text1");
        iUserRoleService.save(userRole1);
        Deparment deparment2=new Deparment();
        deparment2.setName("部门2");
        this.baseMapper.insert(deparment2);
        //int i=1/0;
        UserDomain userUpdate =  new UserDomain();
        userUpdate.setUserId(1000);
        userUpdate.setUserName("userNameUpdate001");
        userUpdate.setPassword("passwordUpdate001");
        userUpdate.setPhone("phoneUpdate001");
        int update =userService.update(userUpdate);
        UserDomain userSave =  new UserDomain();
        userSave.setUserName("userNameSave001");
        userSave.setPassword("passwordSave001");
        userSave.setPhone("phoneSave001");
        int save=userService.insert(userSave);
        //int k=1/0;
    }


    @Override
    public Page listPage(Page paramPage, Map map) {
         paramPage.setRecords(this.baseMapper.listPage(paramPage,map));
        return paramPage;
    }

    @Override
  //  @TxTransaction(isStart=true)
    @Transactional
    public void testTransactionalLcn() {
        Map m =new HashMap();
        m.put("userName","username-916-1");
        m.put("password","password-916-1");
        m.put("phone","phone-916-1");
      /*  UserDomain userDomain = new UserDomain();
        userDomain.setUserName("username-916-1");
        userDomain.setPassword("-916-2");
        userDomain.setPhone("phone-916-3");*/
        LOGGER.info("http://demo2/user/add-int"+JSON.toJSONString(m));
        Map machstatusdetail = CloudClientUtil.postJson("http://demo2"  + "/user/add", m, Map.class);
        LOGGER.info("http://demo2/user/add-out"+JSON.toJSONString(machstatusdetail));
        Deparment deparment2=new Deparment();
        deparment2.setName("部门22222");
        this.baseMapper.insert(deparment2);
        int yy=1/0;
    }
}
