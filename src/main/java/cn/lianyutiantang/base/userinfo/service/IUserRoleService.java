package cn.lianyutiantang.base.userinfo.service;

import cn.lianyutiantang.base.userinfo.entity.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhengjl
 * @since 2018-09-16
 */
public interface IUserRoleService extends IService<UserRole> {

}
