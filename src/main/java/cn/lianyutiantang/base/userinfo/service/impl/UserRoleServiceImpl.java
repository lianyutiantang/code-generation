package cn.lianyutiantang.base.userinfo.service.impl;

import cn.lianyutiantang.base.userinfo.entity.UserRole;
import cn.lianyutiantang.base.userinfo.mapper.UserRoleDao;
import cn.lianyutiantang.base.userinfo.service.IUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhengjl
 * @since 2018-09-16
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleDao, UserRole> implements IUserRoleService {

}
