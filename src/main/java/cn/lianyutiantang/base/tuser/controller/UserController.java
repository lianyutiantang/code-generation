package cn.lianyutiantang.base.tuser.controller;

import cn.lianyutiantang.base.tuser.model.UserDomain;
import cn.lianyutiantang.base.tuser.service.UserService;
//import com.codingapi.tx.annotation.TxTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2017/8/16.
 */
@Controller
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private UserService userService;

    @ResponseBody
    @PostMapping("/add")
    @Transactional
   // @TxTransaction
    public Object addUser(@RequestBody UserDomain user){
        try {
            int i = userService.addUser(user);
            Map map =new HashMap<>();
            map.put("code","1");
            map.put("msg","ok");
            return map;
        }catch (Exception e){
            throw new RuntimeException("zjl异常！");
        }

    }

    @ResponseBody
    @PostMapping("/update")
    public int update(UserDomain user){
        if(user.getUserId()==null){
            return 0;
        }
        return userService.update(user);
    }

    @ResponseBody
    @GetMapping("/all")
    public Object findAllUser(
            @RequestParam(name = "pageNum", required = false, defaultValue = "1")
                    int pageNum,
            @RequestParam(name = "pageSize", required = false, defaultValue = "10")
                    int pageSize){
        return userService.findAllUser(pageNum,pageSize);
    }

    @ResponseBody
    @GetMapping("/testTransactional")
    public Object testTransactional(){

        userService.testTransactional();
        return null;
    }



}
