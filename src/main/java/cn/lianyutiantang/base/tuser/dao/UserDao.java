package cn.lianyutiantang.base.tuser.dao;


import cn.lianyutiantang.base.tuser.model.UserDomain;

import java.util.List;

public interface UserDao {


    int insert(UserDomain record);



    List<UserDomain> selectUsers();

    int update(UserDomain user);
}