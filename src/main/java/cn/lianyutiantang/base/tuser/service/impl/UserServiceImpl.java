package cn.lianyutiantang.base.tuser.service.impl;


import cn.lianyutiantang.base.tuser.model.UserDomain;
import cn.lianyutiantang.base.tuser.service.UserService;
import cn.lianyutiantang.base.tuser.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Administrator on 2017/8/16.
 */
@Service(value = "userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;//这里会报错，但是并不会影响

    @Override
    public int addUser(UserDomain user) {

        return userDao.insert(user);
    }

    /*
    * 这个方法中用到了我们开头配置依赖的分页插件pagehelper
    * 很简单，只需要在service层传入参数，然后将参数传递给一个插件的一个静态方法即可；
    * pageNum 开始页数
    * pageSize 每页显示的数据条数
    * */
    @Override
    public List<UserDomain> findAllUser(int pageNum, int pageSize) {
        //将参数传给这个方法就可以实现物理分页了，非常简单。
        List<UserDomain> userDomains = userDao.selectUsers();
        return userDomains;
    }

    @Override
    public int update(UserDomain user) {
        return userDao.update(user);
    }

    @Override
    public int insert(UserDomain userSave) {
        return userDao.insert(userSave);
    }

    @Override
    @Transactional
    public void testTransactional() {
        UserDomain userUpdate =  new UserDomain();
        userUpdate.setUserId(1000);
        userUpdate.setUserName("userNameUpdate001");
        userUpdate.setPassword("passwordUpdate001");
        userUpdate.setPhone("phoneUpdate001");
        int update =userDao.update(userUpdate);
        //int k=1/0;
        UserDomain userSave =  new UserDomain();
        userSave.setUserName("userNameSave001");
        userSave.setPassword("passwordSave001");
        userSave.setPhone("phoneSave001");
        int save=userDao.insert(userSave);
        //int k=1/0;
    }


}
