package cn.lianyutiantang.base.tuser.service;


import cn.lianyutiantang.base.tuser.model.UserDomain;

import java.util.List;

/**
 * Created by Administrator on 2018/4/19.
 */
public interface UserService {

    int addUser(UserDomain user);

    List<UserDomain> findAllUser(int pageNum, int pageSize);

    int update(UserDomain user);

    void testTransactional();


    int insert(UserDomain userSave);
}
