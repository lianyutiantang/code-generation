package cn.lianyutiantang.base.commons;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author zhengjl
 * @ClassName MybatisPlusConfig
 * @Description mybatisplugin 分页插件配置
 * @Date 2018/9/1618:37
 * @Version 1.0
 **/
@EnableTransactionManagement
@Configuration
@MapperScan({"cn.lianyutiantang.**.dao","cn.lianyutiantang.**.mapper"})
public class MybatisPlusConfig {
    /**
     * 分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }
}
