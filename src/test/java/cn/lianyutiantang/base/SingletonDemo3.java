package cn.lianyutiantang.base;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

/**静态内部类实现模式（线程安全，调用效率高，可以延时加载）
 * @author zhengjl
 * @create 2018-10-17 9:16
 */
public class SingletonDemo3 {
    private String id ;
    private String Name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    private static class SingletonClassInstance{
        private static final SingletonDemo3 instance=new SingletonDemo3();
    }

    private SingletonDemo3(){
        System.out.println("无参构造行数初始化");
    }

    public static SingletonDemo3 getInstance(){
        return SingletonClassInstance.instance;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this, new SerializerFeature[]{SerializerFeature.WriteMapNullValue, SerializerFeature.WriteNullListAsEmpty,
                SerializerFeature.WriteNullStringAsEmpty, SerializerFeature.WriteNullNumberAsZero, SerializerFeature.WriteNullBooleanAsFalse,
                SerializerFeature.UseISO8601DateFormat });
    }

    public static void main(String[] args) {
        SingletonDemo3 singletonDemo1=SingletonDemo3.getInstance();
        SingletonDemo3 singletonDemo2=SingletonDemo3.getInstance();
        if(singletonDemo1==singletonDemo2){
            System.out.println("单例确认！");
            System.out.println(singletonDemo1.toString());
        }
    }

}
